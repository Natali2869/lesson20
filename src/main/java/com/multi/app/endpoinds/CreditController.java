package com.multi.app.endpoinds;

import com.multi.app.db.DBManager;
import com.multi.app.dto.BooleanResponse;
import com.multi.app.dto.CreditRequestDto;
import com.multi.app.dto.CreditRequestPojo;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Tag(name = "CREDIT", description = "API for credit")
@RestController
public class CreditController {
    @Autowired
    private DBManager dbManager;

    @PostMapping(path = "/credit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long addCredit(@RequestBody CreditRequestDto creditRequest) {
        System.out.println(creditRequest.getName());
        if (!isValid(creditRequest)) {
            return 0L;
        }
        System.out.println(creditRequest.getName());

        CreditRequestPojo creditRequestPojo = new CreditRequestPojo();
        creditRequestPojo.setAge(creditRequest.getAge());
        creditRequestPojo.setName(creditRequest.getName());
        creditRequestPojo.setAmount(creditRequest.getAmount());

        boolean result = dbManager.save(creditRequestPojo);

        return result ? creditRequestPojo.getId() : 0L;
    }

    @GetMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getCredit(@PathVariable("id") long id) {
        CreditRequestPojo creditRequest = dbManager.get(id);

        return creditRequest == null ? "\"Not found\"" : creditRequest;
    }

    @DeleteMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object deleteCredit(@PathVariable("id") long id) {
        boolean deleted = dbManager.delete(id);

        BooleanResponse br = new BooleanResponse(deleted);

        return br;
    }

    @GetMapping(path = "/credits", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getCredits() {
        return dbManager.get();
    }

    private boolean isValid(CreditRequestDto creditRequest) {
        if (creditRequest == null) {
            return false;
        }

        if (creditRequest.getName() == null || creditRequest.getName().trim().length() == 0) {
            return false;
        }

        if (creditRequest.getAge() <= 0) {
            return false;
        }

        if (creditRequest.getAmount() <= 0) {
            return false;
        }

        return true;
    }
}
