package com.multi.app.db;


import com.multi.app.dto.CreditRequestPojo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component
public class DBManager {
    private Properties props;
    private SessionFactory sessionFactory;

    public DBManager() {
        fillProps();
        createSessionFactory();
    }

    private void createSessionFactory() {
        sessionFactory = new Configuration()
                .addProperties(props)
                .addAnnotatedClass(CreditRequestPojo.class)
                .buildSessionFactory();
    }

    private void fillProps() {
        props = new Properties();

        props.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/postgres");
        props.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        props.setProperty("hibernate.connection.username", "postgres");
        props.setProperty("hibernate.connection.password", "tursunzade");
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL10Dialect");
        props.setProperty("hibernate.show_sql", "true");
    }

    public boolean save(CreditRequestPojo creditRequestPojo) {
        boolean result;

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Serializable resp = session.save(creditRequestPojo);
            session.getTransaction().commit();

            result = resp instanceof Long && ((Long) resp) > 0;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            result = false;
        }

        return result;
    }

    public CreditRequestPojo get(long id) {
        CreditRequestPojo result;

        try (Session session = sessionFactory.openSession()) {
            result = session.get(CreditRequestPojo.class, id);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            result = null;
        }

        return result;
    }

    public List<CreditRequestPojo> get() {
        List<CreditRequestPojo> pojos;

        try (Session session = sessionFactory.openSession()) {
            List results = session.createQuery("from CreditRequestPojo").list();
            if (results != null) {
                pojos = new ArrayList<>();
                for (Object obj : results) {
                    pojos.add((CreditRequestPojo) obj);
                }
            } else {
                pojos = null;
            }
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            pojos = null;
        }

        return pojos;
    }

    public CreditRequestPojo getLast() {
        CreditRequestPojo pojo;

        try (Session session = sessionFactory.openSession()) {
            List results = session.createQuery("from CreditRequestPojo cr order by cr.id desc").setMaxResults(1).list();
            if (results != null && results.size() > 0) {
                pojo = (CreditRequestPojo) results.get(0);
            } else {
                pojo = null;
            }
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            pojo = null;
        }

        return pojo;
    }

    public long count() {
        long result;

        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(*) from CreditRequestPojo");
            result = (Long) query.uniqueResult();
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            result = -1;
        }

        return result;
    }

    public boolean delete(long id) {
        boolean result;

        CreditRequestPojo creditRequestPojo = new CreditRequestPojo();
        creditRequestPojo.setId(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(creditRequestPojo);
            session.getTransaction().commit();
            result = true;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            result = false;
        }

        return result;
    }
}
