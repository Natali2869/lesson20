package com.multi.app.endpoinds;

import com.multi.app.db.DBManager;
import com.multi.app.dto.CreditRequestDto;
import com.multi.app.dto.CreditRequestPojo;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DbTests {
    @LocalServerPort
    private Integer port;
    private DBManager dbManager;

    @BeforeEach
    public void setup() {
        RestAssured.baseURI = "http://localhost:" + port;
        if (dbManager == null) {
            dbManager = new DBManager();
        }
    }

    @Test
    public void createCreditRequestTest() {
        // create json
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("age", 30);
        jsonObject.put("name", "Anton");
        jsonObject.put("amount", 1500);

        // check create
        Response response = RestAssured.given()
                .contentType("application/json")
                .body(jsonObject.toString())
                .post("/credit");
        Assertions.assertEquals(200, response.statusCode());

        long newId = response.as(Long.class);
        Assertions.assertTrue(newId > 0);

        CreditRequestPojo creditRequestPojo = dbManager.get(newId);
        Assertions.assertNotNull(creditRequestPojo);

        Assertions.assertEquals(30, creditRequestPojo.getAge());
        Assertions.assertEquals("Anton", creditRequestPojo.getName());
        Assertions.assertEquals(1500, creditRequestPojo.getAmount());
    }

    @Test
    public void getCreditRequestTest() {
        // get last record
        CreditRequestPojo cr = dbManager.getLast();
        Assertions.assertNotNull(cr);

        // check get record
        Response response = RestAssured.given().get("/credit/" + cr.getId());
        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(cr.getId(), response.jsonPath().getLong("id"));
        Assertions.assertEquals(cr.getName(), response.jsonPath().getString("name"));
        Assertions.assertEquals(cr.getAge(), response.jsonPath().getInt("age"));
        Assertions.assertEquals(cr.getAmount(), response.jsonPath().getInt("amount"));
    }

    @Test
    public void getCreditRequestsTest() {
        // check get existing records
        Response response = RestAssured.given().get("/credits");
        Assertions.assertEquals(200, response.statusCode());

        int countRest = response.jsonPath().getList("", CreditRequestDto.class).size();
        long countDb = dbManager.count();

        Assertions.assertEquals(countDb, countRest);
    }

    @Test
    public void deleteCreditRequestTest() {
        // insert new
        CreditRequestPojo cr = new CreditRequestPojo();
        cr.setAge(10);
        cr.setName("Test");
        cr.setAmount(1000);
        Assertions.assertTrue(dbManager.save(cr));
        Assertions.assertTrue(cr.getId() > 0);

        // check delete record
        Response response = RestAssured.given().delete("/credit/" + cr.getId());
        Assertions.assertEquals(200, response.statusCode());

        // db deleted
        Assertions.assertNull(dbManager.get(cr.getId()));
    }
}
