package com.multi.app.endpoinds;

import com.multi.app.dto.CreditRequestDto;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CreditApplicationCallTests {
    @LocalServerPort
    private Integer port;

    @BeforeEach
    public void setup() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    public void createCreditRequestTest() {
        // create json
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("age", 30);
        jsonObject.put("name", "Anton");
        jsonObject.put("amount", 1500);

        // check create
        Response response = RestAssured.given()
                .contentType("application/json")
                .body(jsonObject.toString())
                .post("/credit");
        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertTrue(response.as(Long.class) > 0);
    }

    @Test
    public void getCreditRequestTest() {
        // check get record
        Response response = RestAssured.given().get("/credit/1");
        Assertions.assertEquals(200, response.statusCode());

        // get id
        long id = 0;
        try {
            id = response.jsonPath().getLong("id");
        } catch (Exception e) {}

        // get not found
        String str = null;
        try {
            str = response.as(String.class);
        } catch (Exception e) {}

        Assertions.assertTrue(id == 1 || "Not found".equals(str));
    }

    @Test
    public void getCreditRequestsTest() {
        // check get existing records
        Response response = RestAssured.given().get("/credits");
        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertTrue(response.jsonPath().getList("", CreditRequestDto.class).size() > 0);
    }

    @Test
    public void deleteCreditRequestTest() {
        // check delete record
        Response response = RestAssured.given().delete("/credit/1");
        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertFalse(response.jsonPath().getBoolean("success"));
    }
}
